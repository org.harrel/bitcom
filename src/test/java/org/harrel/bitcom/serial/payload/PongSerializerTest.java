package org.harrel.bitcom.serial.payload;

import org.harrel.bitcom.model.msg.payload.Pong;
import org.harrel.bitcom.serial.SerializerFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

class PongSerializerTest {

    PipedInputStream in;
    PipedOutputStream out;
    SerializerFactory sf;

    @BeforeEach
    void init() throws IOException {
        in = new PipedInputStream();
        out = new PipedOutputStream(in);
        sf = new SerializerFactory();
    }

    @Test
    void serializeLoop() throws IOException {
        Pong[] data = new Pong[]{
                new Pong(0xFFFFFFFFFFFFFFFFL),
                new Pong(0x0),
                new Pong(0x7FFFFFFFFFFFFFFFL),
                new Pong(0xFFFFFFFFFFFFFFF0L),
                new Pong(0xAA00AA00BB00CC00L)
        };

        for (Pong pong : data) {
            sf.getPayloadSerializer(pong).serialize(pong, out);
            Pong read = sf.getPayloadSerializer(pong).deserialize(in);
            Assertions.assertEquals(pong, read);
        }
    }
}