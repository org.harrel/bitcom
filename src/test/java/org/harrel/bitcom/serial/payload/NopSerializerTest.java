package org.harrel.bitcom.serial.payload;

import org.harrel.bitcom.model.msg.payload.GetAddr;
import org.harrel.bitcom.model.msg.payload.MemPool;
import org.harrel.bitcom.model.msg.payload.Verack;
import org.harrel.bitcom.serial.SerializerFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

class NopSerializerTest {

    PipedInputStream in;
    PipedOutputStream out;
    SerializerFactory sf;

    @BeforeEach
    void init() throws IOException {
        in = new PipedInputStream();
        out = new PipedOutputStream(in);
        sf = new SerializerFactory();
    }

    @Test
    void serializeLoop() throws IOException {
        Verack verack = new Verack();
        PayloadSerializer<Verack> verackSer = sf.getPayloadSerializer(verack);
        verackSer.serialize(verack, out);
        Assertions.assertEquals(verack, verackSer.deserialize(in));

        GetAddr getAddr = new GetAddr();
        PayloadSerializer<GetAddr> getAddrSer = sf.getPayloadSerializer(getAddr);
        getAddrSer.serialize(getAddr, out);
        Assertions.assertEquals(getAddr, getAddrSer.deserialize(in));

        MemPool memPool = new MemPool();
        PayloadSerializer<MemPool> memPoolSer = sf.getPayloadSerializer(memPool);
        memPoolSer.serialize(memPool, out);
        Assertions.assertEquals(memPool, memPoolSer.deserialize(in));
    }
}