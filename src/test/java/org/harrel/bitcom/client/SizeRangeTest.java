package org.harrel.bitcom.client;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.config.StandardConfiguration;
import org.harrel.bitcom.io.LimitExceededException;
import org.harrel.bitcom.jmx.JmxSupport;
import org.harrel.bitcom.model.*;
import org.harrel.bitcom.model.msg.payload.*;
import org.harrel.bitcom.serial.SerializerFactory;
import org.harrel.bitcom.serial.SizeRangeException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@Tag("functional")
class SizeRangeTest {

    BitcomServer server;
    InetAddress localAddress;

    @BeforeEach
    void init() throws IOException {
        server = new BitcomServer(8333);
        localAddress = InetAddress.getByName("127.0.0.1");
    }

    @AfterEach
    void tearDown() throws IOException {
        server.close();
    }

    @Test
    void addr() throws Exception {
        CompletableFuture<Exception> receiveFuture = new CompletableFuture<>();
        BitcomClient.builder()
                .withGlobalErrorListener((c, e) -> receiveFuture.complete(e))
                .buildAndConnect();
        var data = new Addr(List.of(
                new NetworkAddress(1, Set.of(Service.NODE_NETWORK), InetAddress.getLocalHost(), 1)
        ));
        byte[] bytes = serializeMessage(data);
        bytes[24] = (byte) 0xFD;
        bytes[25] = (byte) 0xFD;
        bytes[26] = (byte) 0xFD;
        server.send(bytes);
        Exception exception = receiveFuture.get(500, TimeUnit.MILLISECONDS);
        assertEquals(SizeRangeException.class, exception.getClass());
    }

    @Test
    void getBlocks() throws Exception {
        CompletableFuture<Exception> receiveFuture = new CompletableFuture<>();
        BitcomClient.builder()
                .withGlobalErrorListener((c, e) -> receiveFuture.complete(e))
                .buildAndConnect();
        var data = new GetBlocks(1, List.of(Hash.empty()));
        byte[] bytes = serializeMessage(data);
        bytes[28] = (byte) 0xFD;
        bytes[29] = (byte) 0xFD;
        bytes[30] = (byte) 0xFD;
        server.send(bytes);
        Exception exception = receiveFuture.get(500, TimeUnit.MILLISECONDS);
        assertEquals(SizeRangeException.class, exception.getClass());
    }

    @Test
    void getHeaders() throws Exception {
        CompletableFuture<Exception> receiveFuture = new CompletableFuture<>();
        BitcomClient.builder()
                .withGlobalErrorListener((c, e) -> receiveFuture.complete(e))
                .buildAndConnect();
        var data = new GetHeaders(1, List.of(Hash.empty()));
        byte[] bytes = serializeMessage(data);
        bytes[28] = (byte) 0xFD;
        bytes[29] = (byte) 0xFD;
        bytes[30] = (byte) 0xFD;
        server.send(bytes);
        Exception exception = receiveFuture.get(500, TimeUnit.MILLISECONDS);
        assertEquals(SizeRangeException.class, exception.getClass());
    }

    @Test
    void inv() throws Exception {
        CompletableFuture<Exception> receiveFuture = new CompletableFuture<>();
        BitcomClient.builder()
                .withGlobalErrorListener((c, e) -> receiveFuture.complete(e))
                .buildAndConnect();
        var data = new Inv(List.of(new InventoryVector(InventoryVector.Type.MSG_BLOCK, Hash.empty())));
        byte[] bytes = serializeMessage(data);
        bytes[24] = (byte) 0xFD;
        bytes[25] = (byte) 0xFD;
        bytes[26] = (byte) 0xFD;
        server.send(bytes);
        Exception exception = receiveFuture.get(500, TimeUnit.MILLISECONDS);
        assertEquals(SizeRangeException.class, exception.getClass());
    }

    private <T extends Payload> byte[] serializeMessage(T payload) throws Exception {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        MessageSender sender = new MessageSender(bout, new SerializerFactory(), StandardConfiguration.MAIN, JmxSupport.detachedMBean());
        sender.sendMessage(payload).get();
        return bout.toByteArray();
    }
}
