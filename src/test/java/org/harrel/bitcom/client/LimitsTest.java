package org.harrel.bitcom.client;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.config.StandardConfiguration;
import org.harrel.bitcom.io.LimitExceededException;
import org.harrel.bitcom.jmx.JmxSupport;
import org.harrel.bitcom.model.*;
import org.harrel.bitcom.model.msg.payload.*;
import org.harrel.bitcom.serial.SerializerFactory;
import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@Tag("functional")
class LimitsTest {

    BitcomServer server;
    InetAddress localAddress;

    @BeforeEach
    void init() throws IOException {
        server = new BitcomServer(8333);
        localAddress = InetAddress.getByName("127.0.0.1");
    }

    @AfterEach
    void tearDown() throws IOException {
        server.close();
    }

    @Test
    void sendMessageSizeLimit() throws Exception {
        BitcomClient client = BitcomClient.builder()
                .withLimits(new Limits(0, 0, 0, 0, 0))
                .buildAndConnect();
        var future = client.sendMessage(new Ping(1L));
        try {
            future.get();
            fail();
        } catch (ExecutionException e) {
            assertEquals(LimitExceededException.class, e.getCause().getClass());
        }
    }

    @Test
    void sendBlocksSizeLimit() throws Exception {
        BitcomClient client = BitcomClient.builder()
                .withLimits(new Limits(2_000_000, 0, 0, 0, 0))
                .buildAndConnect();
        Headers headers = new Headers(List.of(new Block(1, Hash.empty(), Hash.empty(), 1, 1, 1)));
        var future = client.sendMessage(headers);
        try {
            future.get();
            fail();
        } catch (ExecutionException e) {
            assertTrue(e.getCause().getMessage().contains("Block"));
            assertEquals(LimitExceededException.class, e.getCause().getClass());
        }
    }

    @Test
    void sendTxSizeLimit() throws Exception {
        BitcomClient client = BitcomClient.builder()
                .withLimits(new Limits(2_000_000, 1000, 0, 1000, 1000))
                .buildAndConnect();
        var block = new Block(1, Hash.empty(), Hash.empty(), 1, 1, 1,
                List.of(
                        new Tx(1,
                                List.of(
                                        new TxIn(new OutPoint(1, Hash.empty()), "ff", 1)
                                ),
                                List.of(
                                        new TxOut(1, "ff")
                                ), 1)));
        var future = client.sendMessage(block);
        try {
            future.get();
            fail();
        } catch (ExecutionException e) {
            assertTrue(e.getCause().getMessage().contains("Tx"));
            assertEquals(LimitExceededException.class, e.getCause().getClass());
        }
    }

    @Test
    void sendTxInSizeLimit() throws Exception {
        BitcomClient client = BitcomClient.builder()
                .withLimits(new Limits(2_000_000, 1000, 1000, 0, 1000))
                .buildAndConnect();
        var tx = new Tx(1,
                List.of(
                        new TxIn(new OutPoint(1, Hash.empty()), "ff", 1)
                ),
                List.of(
                        new TxOut(1, "ff")
                ), 1);
        var future = client.sendMessage(tx);
        try {
            future.get();
            fail();
        } catch (ExecutionException e) {
            assertTrue(e.getCause().getMessage().contains("TxIn"));
            assertEquals(LimitExceededException.class, e.getCause().getClass());
        }
    }

    @Test
    void sendTxOutSizeLimit() throws Exception {
        BitcomClient client = BitcomClient.builder()
                .withLimits(new Limits(2_000_000, 1000, 1000, 1000, 0))
                .buildAndConnect();
        var tx = new Tx(1,
                List.of(
                        new TxIn(new OutPoint(1, Hash.empty()), "ff", 1)
                ),
                List.of(
                        new TxOut(1, "ff")
                ), 1);
        var future = client.sendMessage(tx);
        try {
            future.get();
            fail();
        } catch (ExecutionException e) {
            assertTrue(e.getCause().getMessage().contains("TxOut"));
            assertEquals(LimitExceededException.class, e.getCause().getClass());
        }
    }

    @Test
    void receiveMessageSizeLimit() throws Exception {
        CompletableFuture<Exception> receiveFuture = new CompletableFuture<>();
        BitcomClient.builder()
                .withLimits(new Limits(0, 0, 0, 0, 0))
                .withGlobalErrorListener((c, e) -> receiveFuture.complete(e))
                .buildAndConnect();
        var data = new Ping(1);
        server.send(serializeMessage(data));
        Exception exception = receiveFuture.get(500, TimeUnit.MILLISECONDS);
        assertEquals(LimitExceededException.class, exception.getClass());
    }

    @Test
    void receiveBlocksSizeLimit() throws Exception {
        CompletableFuture<Exception> receiveFuture = new CompletableFuture<>();
        BitcomClient.builder()
                .withLimits(new Limits(20_000, 0, 1000, 1000, 1000))
                .withGlobalErrorListener((c, e) -> receiveFuture.complete(e))
                .buildAndConnect();
        var data = new Headers(List.of(new Block(1, Hash.empty(), Hash.empty(), 1, 1, 1)));
        server.send(serializeMessage(data));
        Exception exception = receiveFuture.get(500, TimeUnit.MILLISECONDS);
        assertEquals(LimitExceededException.class, exception.getClass());
    }

    @Test
    void receiveTxSizeLimit() throws Exception {
        CompletableFuture<Exception> receiveFuture = new CompletableFuture<>();
        BitcomClient.builder()
                .withLimits(new Limits(20_000, 1000, 0, 1000, 1000))
                .withGlobalErrorListener((c, e) -> receiveFuture.complete(e))
                .buildAndConnect();
        var data = new Block(1, Hash.empty(), Hash.empty(), 1, 1, 1,
                List.of(
                        new Tx(1,
                                List.of(
                                        new TxIn(new OutPoint(1, Hash.empty()), "ff", 1)
                                ),
                                List.of(
                                        new TxOut(1, "ff")
                                ), 1)));
        server.send(serializeMessage(data));
        Exception exception = receiveFuture.get(500, TimeUnit.MILLISECONDS);
        assertEquals(LimitExceededException.class, exception.getClass());
    }

    @Test
    void receiveTxInSizeLimit() throws Exception {
        CompletableFuture<Exception> receiveFuture = new CompletableFuture<>();
        BitcomClient.builder()
                .withLimits(new Limits(20_000, 1000, 1000, 0, 1000))
                .withGlobalErrorListener((c, e) -> receiveFuture.complete(e))
                .buildAndConnect();
        var data = new Tx(1,
                List.of(
                        new TxIn(new OutPoint(1, Hash.empty()), "ff", 1)
                ),
                List.of(
                        new TxOut(1, "ff")
                ), 1);
        server.send(serializeMessage(data));
        Exception exception = receiveFuture.get(500, TimeUnit.MILLISECONDS);
        assertEquals(LimitExceededException.class, exception.getClass());
    }

    @Test
    void receiveTxOutSizeLimit() throws Exception {
        CompletableFuture<Exception> receiveFuture = new CompletableFuture<>();
        BitcomClient.builder()
                .withLimits(new Limits(20_000, 1000, 1000, 1000, 0))
                .withGlobalErrorListener((c, e) -> receiveFuture.complete(e))
                .buildAndConnect();
        var data = new Tx(1,
                List.of(
                        new TxIn(new OutPoint(1, Hash.empty()), "ff", 1)
                ),
                List.of(
                        new TxOut(1, "ff")
                ), 1);
        server.send(serializeMessage(data));
        Exception exception = receiveFuture.get(500, TimeUnit.MILLISECONDS);
        assertEquals(LimitExceededException.class, exception.getClass());
    }

    private <T extends Payload> byte[] serializeMessage(T payload) throws Exception {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        MessageSender sender = new MessageSender(bout, new SerializerFactory(), StandardConfiguration.MAIN, JmxSupport.detachedMBean());
        sender.sendMessage(payload).get();
        return bout.toByteArray();
    }
}
