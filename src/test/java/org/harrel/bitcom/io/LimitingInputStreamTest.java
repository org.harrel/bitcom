package org.harrel.bitcom.io;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class LimitingInputStreamTest {

    @Test
    void limit0() {
        byte[] data = new byte[8];
        var bais = new ByteArrayInputStream(data);
        var lis = new LimitingInputStream(bais, 0);
        assertThrows(LimitExceededException.class, lis::read);
    }

    @Test
    void limit1() throws IOException {
        byte first = (byte) 196;
        byte[] data = new byte[8];
        data[0] = first;
        var bais = new ByteArrayInputStream(data);
        var lis = new LimitingInputStream(bais, 1);
        assertEquals(first, (byte) (lis.read() & 0xFF));
        assertThrows(LimitExceededException.class, lis::read);
    }

    @Test
    void limit23() throws IOException {
        byte[] data = new byte[24];
        byte[] buff = new byte[8];
        Arrays.fill(data, (byte) 125);
        data[0] = 1;
        var bais = new ByteArrayInputStream(data);
        var lis = new LimitingInputStream(bais, 23);
        assertArrayEquals(Arrays.copyOf(data, 8), lis.readNBytes(8));
        assertEquals(8, lis.read(buff, 0, 8));
        assertArrayEquals(Arrays.copyOfRange(data, 8, 16), buff);
        assertThrows(LimitExceededException.class, () -> lis.readNBytes(8));
    }
}