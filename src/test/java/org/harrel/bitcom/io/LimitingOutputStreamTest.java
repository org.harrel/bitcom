package org.harrel.bitcom.io;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import static org.junit.jupiter.api.Assertions.*;

class LimitingOutputStreamTest {

    @Test
    void limit0() {
        var baos = new ByteArrayOutputStream();
        var lis = new LimitingOutputStream(baos, 0);
        assertThrows(LimitExceededException.class, () -> lis.write(0));
    }

    @Test
    void limit1() {
        var baos = new ByteArrayOutputStream();
        var lis = new LimitingOutputStream(baos, 1);
        assertDoesNotThrow(() -> lis.write(1));
        assertThrows(LimitExceededException.class, () -> lis.write(0));
    }

    @Test
    void limit15() {
        var baos = new ByteArrayOutputStream();
        var lis = new LimitingOutputStream(baos, 15);
        assertDoesNotThrow(() -> lis.write(new byte[16], 8 ,8));
        assertThrows(LimitExceededException.class, () -> lis.write(new byte[8]));
    }
}