package org.harrel.bitcom.io;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TeeInputStreamTest {

    @Test
    void read() throws IOException {
        byte[] data = new byte[]{-10, 0, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, -128, 127};
        var in = new ByteArrayInputStream(data);
        var out = new ByteArrayOutputStream();
        var tee = new TeeInputStream(in, out);
        int read = tee.read();
        assertEquals(data[0], (byte) (read & 0xFF));
        assertEquals(1, out.toByteArray().length);
        assertEquals(data[0], out.toByteArray()[0]);
        tee.read(new byte[24], 0, 24);
        assertArrayEquals(data, out.toByteArray());
    }
}