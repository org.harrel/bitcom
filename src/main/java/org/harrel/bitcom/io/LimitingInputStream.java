package org.harrel.bitcom.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class LimitingInputStream extends FilterInputStream {

    private final int limit;
    private int bytesRead;

    public LimitingInputStream(InputStream in, int limit) {
        super(in);
        this.limit = limit;
    }

    @Override
    public int read() throws IOException {
        int read = super.read();
        bytesRead++;
        checkLimit();
        return read;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        int read = super.read(b, off, len);
        bytesRead += read;
        checkLimit();
        return read;
    }

    private void checkLimit() {
        if (bytesRead > limit) {
            throw new LimitExceededException("Input stream read limit exceeded. limit=" + limit);
        }
    }
}
