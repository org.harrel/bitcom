package org.harrel.bitcom.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class LimitingOutputStream extends FilterOutputStream {

    private final int limit;
    private int bytesWrote;

    public LimitingOutputStream(OutputStream out, int limit) {
        super(out);
        this.limit = limit;
    }

    @Override
    public void write(int b) throws IOException {
        bytesWrote++;
        checkLimit();
        super.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        bytesWrote += len;
        checkLimit();
        out.write(b, off, len);
    }

    private void checkLimit() {
        if (bytesWrote > limit) {
            throw new LimitExceededException("Output stream write limit exceeded. limit=" + limit);
        }
    }
}
