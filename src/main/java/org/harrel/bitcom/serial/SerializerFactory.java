package org.harrel.bitcom.serial;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.model.msg.payload.*;
import org.harrel.bitcom.serial.payload.*;

public class SerializerFactory {

    private final Limits limits;

    public SerializerFactory() {
        this(Limits.DEFAULT);
    }

    public SerializerFactory(Limits limits) {
        this.limits = limits;
    }

    public HeaderSerializer getHeaderSerializer() {
        return new HeaderSerializer(limits);
    }

    @SuppressWarnings("unchecked")
    public <T extends Payload> PayloadSerializer<T> getPayloadSerializer(T payload) {
        return (PayloadSerializer<T>) getPayloadSerializer(payload.getCommand());
    }

    public PayloadSerializer<?> getPayloadSerializer(Command cmd) {
        return switch (cmd) {
            case VERSION -> new VersionSerializer(limits);
            case VERACK -> new NopSerializer<>(limits, Verack::new);
            case ADDR -> new AddrSerializer(limits);
            case INV -> new InventorySerializer<>(limits, Inv::new);
            case GETDATA -> new InventorySerializer<>(limits, GetData::new);
            case NOTFOUND -> new InventorySerializer<>(limits, NotFound::new);
            case GETBLOCKS -> new GetBlocksSerializer(limits);
            case GETHEADERS -> new GetHeadersSerializer(limits);
            case TX -> new TxSerializer(limits);
            case BLOCK -> new BlockSerializer(limits);
            case HEADERS -> new HeadersSerializer(limits);
            case GETADDR -> new NopSerializer<>(limits, GetAddr::new);
            case MEMPOOL -> new NopSerializer<>(limits, MemPool::new);
            case PING -> new PingSerializer(limits);
            case PONG -> new PongSerializer(limits);
            case REJECT -> new RejectSerializer(limits);
        };
    }
}
