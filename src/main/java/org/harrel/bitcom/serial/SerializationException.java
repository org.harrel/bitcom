package org.harrel.bitcom.serial;

public class SerializationException extends RuntimeException {
    public SerializationException(String message) {
        super(message);
    }
}
