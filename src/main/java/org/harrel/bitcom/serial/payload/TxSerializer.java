package org.harrel.bitcom.serial.payload;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.model.msg.payload.Tx;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class TxSerializer extends PayloadSerializer<Tx> {

    public TxSerializer(Limits limits) {
        super(limits);
    }

    @Override
    public void serializeInternal(Tx payload, OutputStream out) throws IOException {
        writeTx(payload, out);
    }

    @Override
    public Tx deserializeInternal(InputStream in) throws IOException {
        return readTx(in);
    }
}
