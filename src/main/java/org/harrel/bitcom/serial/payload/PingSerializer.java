package org.harrel.bitcom.serial.payload;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.model.msg.payload.Ping;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PingSerializer extends PayloadSerializer<Ping> {

    public PingSerializer(Limits limits) {
        super(limits);
    }

    @Override
    public void serializeInternal(Ping payload, OutputStream out) throws IOException {
        writeInt64LE(payload.nonce(), out);
    }

    @Override
    public Ping deserializeInternal(InputStream in) throws IOException {
        return new Ping(readInt64LE(in));
    }
}
