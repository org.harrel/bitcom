package org.harrel.bitcom.serial.payload;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.io.LimitExceededException;
import org.harrel.bitcom.io.LimitingInputStream;
import org.harrel.bitcom.io.LimitingOutputStream;
import org.harrel.bitcom.model.Hash;
import org.harrel.bitcom.model.TxIn;
import org.harrel.bitcom.model.TxOut;
import org.harrel.bitcom.model.msg.payload.Block;
import org.harrel.bitcom.model.msg.payload.Payload;
import org.harrel.bitcom.model.msg.payload.Tx;
import org.harrel.bitcom.serial.Serializer;
import org.harrel.bitcom.util.Lists;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class PayloadSerializer<T extends Payload> extends Serializer<T> {

    protected PayloadSerializer(Limits limits) {
        super(limits);
    }

    @Override
    public final void serialize(T payload, OutputStream out) throws IOException {
        serializeInternal(payload, new LimitingOutputStream(out, limits.messageSize()));
    }

    @Override
    public final T deserialize(InputStream in) throws IOException {
        return deserializeInternal(new LimitingInputStream(in, limits.messageSize()));
    }

    abstract void serializeInternal(T payload, OutputStream out) throws IOException;

    abstract T deserializeInternal(InputStream in) throws IOException;

    protected void writeBlock(Block payload, OutputStream out) throws IOException {
        checkTxLimit(payload.transactions().size());
        writeInt32LE(payload.version(), out);
        writeHash(payload.previous(), out);
        writeHash(payload.merkleRoot(), out);
        writeInt32LE(payload.timestamp(), out);
        writeInt32LE(payload.bits(), out);
        writeInt32LE(payload.nonce(), out);
        writeVarInt(payload.transactions().size(), out);
        for (Tx tx : payload.transactions()) {
            writeTx(tx, out);
        }
    }

    protected Block readBlock(InputStream in) throws IOException {
        int version = readInt32LE(in);
        Hash previous = readHash(in);
        Hash merkleRoot = readHash(in);
        int timestamp = readInt32LE(in);
        int bits = readInt32LE(in);
        int nonce = readInt32LE(in);
        int txCount = (int) readVarInt(in);
        checkTxLimit(txCount);
        Tx[] txs = new Tx[txCount];
        for (int i = 0; i < txCount; i++) {
            txs[i] = readTx(in);
        }
        return new Block(version, previous, merkleRoot, timestamp, bits, nonce, Lists.fromArray(txs));
    }

    protected void writeTx(Tx payload, OutputStream out) throws IOException {
        checkTxInLimit(payload.inputs().size());
        checkTxOutLimit(payload.outputs().size());
        writeInt32LE(payload.version(), out);
        writeVarInt(payload.inputs().size(), out);
        for (TxIn input : payload.inputs()) {
            writeTxIn(input, out);
        }
        writeVarInt(payload.outputs().size(), out);
        for (TxOut output : payload.outputs()) {
            writeTxOut(output, out);
        }
        writeInt32LE(payload.lockTime(), out);
    }

    protected Tx readTx(InputStream in) throws IOException {
        int version = readInt32LE(in);
        int inputsCount = (int) readVarInt(in);
        checkTxInLimit(inputsCount);
        TxIn[] inputs = new TxIn[inputsCount];
        for (int i = 0; i < inputsCount; i++) {
            inputs[i] = readTxIn(in);
        }
        int outputsCount = (int) readVarInt(in);
        checkTxOutLimit(outputsCount);
        TxOut[] outputs = new TxOut[outputsCount];
        for (int i = 0; i < outputsCount; i++) {
            outputs[i] = readTxOut(in);
        }
        int lockTime = readInt32LE(in);
        return new Tx(version, Lists.fromArray(inputs), Lists.fromArray(outputs), lockTime);
    }

    private void checkTxLimit(int size) {
        if (size > limits.txSize()) {
            throw new LimitExceededException("Tx limit exceeded. limit=" + limits.txSize());
        }
    }

    private void checkTxInLimit(int size) {
        if (size > limits.txInSize()) {
            throw new LimitExceededException("TxIn limit exceeded. limit=" + limits.txInSize());
        }
    }

    private void checkTxOutLimit(int size) {
        if (size > limits.txOutSize()) {
            throw new LimitExceededException("TxOut limit exceeded. limit=" + limits.txOutSize());
        }
    }
}
