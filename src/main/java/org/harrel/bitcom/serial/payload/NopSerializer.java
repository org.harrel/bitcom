package org.harrel.bitcom.serial.payload;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.model.msg.payload.Payload;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.function.Supplier;

public class NopSerializer<T extends Payload> extends PayloadSerializer<T> {

    private final Supplier<T> constructor;

    public NopSerializer(Limits limits, Supplier<T> constructor) {
        super(limits);
        this.constructor = constructor;
    }

    @Override
    public void serializeInternal(T payload, OutputStream out) {
        /* Nothing to do */
    }

    @Override
    public T deserializeInternal(InputStream in) {
        return constructor.get();
    }
}
