package org.harrel.bitcom.serial.payload;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.model.msg.payload.Pong;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PongSerializer extends PayloadSerializer<Pong> {

    public PongSerializer(Limits limits) {
        super(limits);
    }

    @Override
    public void serializeInternal(Pong payload, OutputStream out) throws IOException {
        writeInt64LE(payload.nonce(), out);
    }

    @Override
    public Pong deserializeInternal(InputStream in) throws IOException {
        return new Pong(readInt64LE(in));
    }
}
