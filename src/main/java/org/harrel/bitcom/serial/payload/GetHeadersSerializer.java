package org.harrel.bitcom.serial.payload;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.model.Hash;
import org.harrel.bitcom.model.msg.payload.GetHeaders;
import org.harrel.bitcom.serial.SizeRangeException;
import org.harrel.bitcom.util.Lists;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class GetHeadersSerializer extends PayloadSerializer<GetHeaders> {

    public GetHeadersSerializer(Limits limits) {
        super(limits);
    }

    @Override
    public void serializeInternal(GetHeaders payload, OutputStream out) throws IOException {
        writeInt32LE(payload.version(), out);
        writeVarInt(payload.hashes().size(), out);
        for (Hash hash : payload.hashes()) {
            writeHash(hash, out);
        }
        writeHash(payload.stopHash(), out);
    }

    @Override
    public GetHeaders deserializeInternal(InputStream in) throws IOException {
        int version = readInt32LE(in);
        int count = (int) readVarInt(in);
        if (count > GetHeaders.SIZE_RANGE.max()) {
            throw new SizeRangeException("GetHeaders message must contain valid number of block hashes", GetHeaders.SIZE_RANGE, count);
        }

        Hash[] hashes = new Hash[count];
        for (int i = 0; i < count; i++) {
            hashes[i] = readHash(in);
        }
        var stopHash = readHash(in);
        return new GetHeaders(version, Lists.fromArray(hashes), stopHash);
    }
}
