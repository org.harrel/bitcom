package org.harrel.bitcom.serial.payload;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.model.msg.payload.Block;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BlockSerializer extends PayloadSerializer<Block> {

    public BlockSerializer(Limits limits) {
        super(limits);
    }

    @Override
    public void serializeInternal(Block payload, OutputStream out) throws IOException {
        writeBlock(payload, out);
    }

    @Override
    public Block deserializeInternal(InputStream in) throws IOException {
        return readBlock(in);
    }
}
