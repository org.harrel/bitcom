package org.harrel.bitcom.serial.payload;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.model.NetworkAddress;
import org.harrel.bitcom.model.msg.payload.Addr;
import org.harrel.bitcom.serial.SizeRangeException;
import org.harrel.bitcom.util.Lists;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class AddrSerializer extends PayloadSerializer<Addr> {

    public AddrSerializer(Limits limits) {
        super(limits);
    }

    @Override
    public void serializeInternal(Addr payload, OutputStream out) throws IOException {
        writeVarInt(payload.addresses().size(), out);
        for (NetworkAddress address : payload.addresses()) {
            writeNetworkAddress(address, out);
        }
    }

    @Override
    public Addr deserializeInternal(InputStream in) throws IOException {
        int count = (int) readVarInt(in);
        if (count > Addr.SIZE_RANGE.max()) {
            throw new SizeRangeException("Addr message must contain valid number of addresses", Addr.SIZE_RANGE, count);
        }

        NetworkAddress[] addresses = new NetworkAddress[count];
        for (int i = 0; i < count; i++) {
            addresses[i] = readNetworkAddress(in);
        }
        return new Addr(Lists.fromArray(addresses));
    }
}
