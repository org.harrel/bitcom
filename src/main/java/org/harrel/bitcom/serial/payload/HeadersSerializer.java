package org.harrel.bitcom.serial.payload;

import org.harrel.bitcom.config.Limits;
import org.harrel.bitcom.io.LimitExceededException;
import org.harrel.bitcom.model.msg.payload.Block;
import org.harrel.bitcom.model.msg.payload.Headers;
import org.harrel.bitcom.util.Lists;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class HeadersSerializer extends PayloadSerializer<Headers> {

    public HeadersSerializer(Limits limits) {
        super(limits);
    }

    @Override
    public void serializeInternal(Headers payload, OutputStream out) throws IOException {
        checkBlockLimit(payload.blocks().size());
        writeVarInt(payload.blocks().size(), out);
        for (Block block : payload.blocks()) {
            writeBlock(block, out);
        }
    }

    @Override
    public Headers deserializeInternal(InputStream in) throws IOException {
        int blockCount = (int) readVarInt(in);
        checkBlockLimit(blockCount);
        Block[] blocks = new Block[blockCount];
        for (int i = 0; i < blockCount; i++) {
            blocks[i] = readBlock(in);
        }
        return new Headers(Lists.fromArray(blocks));
    }

    private void checkBlockLimit(int size) {
        if (size > limits.blockSize()) {
            throw new LimitExceededException("Block limit exceeded. limit=" + limits.blockSize());
        }
    }
}
