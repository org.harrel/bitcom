package org.harrel.bitcom.serial;

import org.harrel.bitcom.util.Range;

public class SizeRangeException extends SerializationException {
    public SizeRangeException(String msg, Range range, long size) {
        super("%s. range=%s, was=%d".formatted(msg, range, size));
    }
}
