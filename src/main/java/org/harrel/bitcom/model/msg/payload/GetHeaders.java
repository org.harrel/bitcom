package org.harrel.bitcom.model.msg.payload;

import org.harrel.bitcom.model.Hash;
import org.harrel.bitcom.util.Range;

import java.util.Collection;

public record GetHeaders(int version, Collection<Hash> hashes, Hash stopHash) implements Payload {

    public static final Range SIZE_RANGE = new Range(1, 2000);

    public GetHeaders {
        if (hashes == null || hashes.isEmpty() || hashes.size() > SIZE_RANGE.max()) {
            throw new IllegalArgumentException("GetHeaders message must contain valid number of block hashes " + SIZE_RANGE);
        }
        if (stopHash == null) {
            throw new IllegalArgumentException("StopHash cannot be null");
        }
    }

    public GetHeaders(int version, Collection<Hash> hashes) {
        this(version, hashes, Hash.empty());
    }
}
