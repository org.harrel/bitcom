package org.harrel.bitcom.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Lists {

    Lists() {}

    /* List.of(array) copies whole array and this is more efficient */
    public static <T> List<T> fromArray(T[] arr) {
        return Collections.unmodifiableList(Arrays.asList(arr));
    }
}
