package org.harrel.bitcom.config;

/**
 * @param messageSize max message size in bytes, not counting header
 * @param blockSize quantity of {@link org.harrel.bitcom.model.msg.payload.Block} objects in {@link org.harrel.bitcom.model.msg.payload.Headers} message
 * @param txSize quantity of {@link org.harrel.bitcom.model.msg.payload.Tx} objects in {@link org.harrel.bitcom.model.msg.payload.Block} message
 * @param txInSize quantity of {@link org.harrel.bitcom.model.TxIn} objects in {@link org.harrel.bitcom.model.msg.payload.Tx} message
 * @param txOutSize quantity of {@link org.harrel.bitcom.model.TxOut} objects in {@link org.harrel.bitcom.model.msg.payload.Tx} message
 */
public record Limits(int messageSize,
                     int blockSize,
                     int txSize,
                     int txInSize,
                     int txOutSize) {

    public static final Limits DEFAULT = new Limits(
            4_194_304,
            30_000,
            30_000,
            10_000,
            10_000);
}
